"""
Django settings for gsorter project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import djcelery
djcelery.setup_loader()


BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$bq0!f_izgt+6=c!hqbj#3+b*2de@&zjo7wbwp6q2&6^0aq*^9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    # FIXME: For now gsorter.themes is hardcoded here, Need to fix this
    'gsorter.themes.default',
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

# To import gsorter core apps.
from gsorter import get_core_apps
INSTALLED_APPS += get_core_apps(['apps.clientapp'])


ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gsorter_flipkart',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'uploads')
MEDIA_URL = '/media/'


STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

RAW_IMAGES_PATH = '/home/codespresso/Pictures/'
FINAL_IMAGES_PATH = os.path.join(MEDIA_ROOT, 'shipments')

# Overriding django session engine for web socket
# Using redis_sessions
SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_PREFIX = 'session'

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

# ==============
# Gsorter settings
# ==============

from gsorter.defaults import *
# If you want to override GSorter default settings then override here.

MANIFEST_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'manifests')

# Import Logging Config, Specify any settings before this line
from .logging import *

RAVEN_CONFIG = {}


# Flipkart Specific
FACTOR_FOR_ANY = ['*']

CLIENT_NAME = 'Flipkart'

UNITS = {
    "breadth": "cm",
    "length": "cm",
    "height": "cm",
    "weight": "gm",
    "volume": "cm3"
}

THRESHOLD_DIMENSIONS = {
    'length': 70,
    'breadth': 70,
    'height': 70
}

ENABLE_EMPTY_AWB_NOTIFICTIONS = False
